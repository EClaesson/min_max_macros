//! Macros for nicer min and max.
//! Also contains macros for min/max of f64s.

/// Returns the lowest value (Ord) of the passed parameters.
///
/// # Examples
/// ```
/// min!(3, 7, 2, 5);
/// = 2
/// ```
#[macro_export]
macro_rules! min {
    ($x: expr) => ($x);
    ($x: expr, $($xs: expr), +) => {
        {
            use std::cmp::min;
            min($x, min!($($xs), +))
        }
    }
}

/// Returns the highest value (Ord) of the passed parameters.
///
/// # Examples
/// ```
/// max!(3, 7, 2, 5);
/// = 7
/// ```
#[macro_export]
macro_rules! max {
    ($x: expr) => ($x);
    ($x: expr, $($xs: expr), +) => {
        {
            use std::cmp::max;
            max($x, max!($($xs), +))
        }
    }
}

/// f(x, y) form of f64's x.min(y)
/// Used in min_f!
#[allow(dead_code)]
pub fn min_f64(x: f64, y: f64) -> f64 {
    x.min(y)
}

/// f(x, y) form of f64's x.max(y)
/// Used in max_f!
#[allow(dead_code)]
pub fn max_f64(x: f64, y: f64) -> f64 {
    x.max(y)
}

/// Returns the lowest value (f64) of the passed parameters.
///
/// # Examples
/// ```
/// min_f!(3.1, 7.6, 2.3, 5.0);
/// = 2.3
/// ```
#[macro_export]
macro_rules! min_f {
    ($x: expr) => ($x as f64);
    ($x: expr, $($xs: expr), +) => {
        {
            use min_max_macros::min_f64;
            min_f64($x as f64, min_f!($($xs), +))
        }
    }
}

/// Returns the highest value (f64) of the passed parameters.
///
/// # Examples
/// ```
/// max_f!(3.1, 7.6, 2.3, 5.0);
/// = 7.6
/// ```
#[macro_export]
macro_rules! max_f {
    ($x: expr) => ($x as f64);
    ($x: expr, $($xs: expr), +) => {
        {
            use min_max_macros::max_f64;
            max_f64($x as f64, max_f!($($xs), +))
        }
    }
}

#[test]
fn test_min() {
    assert!(min!(1) == 1);
    assert!(min!(1, 2, 3) == 1);
    assert!(min!(-1, 0, 1) == -1);
}

#[test]
fn test_max() {
    assert!(max!(1) == 1);
    assert!(max!(1, 2, 3) == 3);
    assert!(max!(-1, 0, 1) == 1);
}

//#[test]
//fn test_min_f() {
//    assert!(min_f!(1.0) == 1.0);
//    assert!(min_f!(1.2, 3.4, 5.6) == 1.2);
//    assert!(min_f!(-1.0, 0.0, 1.0) == -1.0);
//}
//
//#[test]
//fn test_max_f() {
//    assert!(max_f!(1.0) == 1.0);
//    assert!(max_f!(1.2, 3.4, 5.6) == 5.6);
//    assert!(max_f!(-1.0, 0.0, 1.0) == 1.0);
//}
