## Macros for nicer min()/max().


Crate: https://crates.io/crates/min_max_macros


### Usage
    min!(10, 2, 5, 9)
	// = 2
	
	max!(10, 2, 5, 9)
	// = 10
	
	min_f!(10.2, 2.5, 5.3, 9.9)
	// = 2.5
	
	max_f!(10.2, 2.5, 5.3, 9.9)
	// = 10.2
